# Instruction #
***Background***

The goal of this experiment is to verify the effect of variance on the length of an M/G/1 queue as utilization ![equation](http://www.sciweavers.org/tex2img.php?eq=%20%5Crho%20&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) approaches 1 follows the formula:![equation](http://www.sciweavers.org/tex2img.php?eq=E%28N%29%3D%5Cfrac%7B%5Crho%7D%7B1-%5Crho%7D%5B1-%5Cfrac%7B%5Crho%7D%7B2%7D%281-%5Cmu%5E2%5Csigma%5E2%29%5D&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) We will do this in two ways: with an ns2 simulation and a testbed experiment.

***Results***

![ns.png](https://bitbucket.org/repo/y4e7oM/images/3173403404-ns.png "ns2 Simulation")

*ns2 Simulation*

![testbed.png](https://bitbucket.org/repo/y4e7oM/images/146507467-testbed.png)

*GENI testbed*

From Figures, the length of three different M/G/1 queue will reach to infinite as utilization ![equation](http://www.sciweavers.org/tex2img.php?eq=%24%5Crho%24&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) approaches to 1, but the range of queue length will increase because the slope of function increase quicker while ![b](http://www.sciweavers.org/tex2img.php?eq=%5Crho%5Cto%201&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) then subtle change in ![equation](http://www.sciweavers.org/tex2img.php?eq=%24%5Crho%24&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) will cause significant change in queue length. And for small ![equation](http://www.sciweavers.org/tex2img.php?eq=%24%5Crho%24&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0), not too different from three different M/G/1 results, but while ![b](http://www.sciweavers.org/tex2img.php?eq=%5Crho%5Cto%201&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0), 1/2 of corresponding M/M/1 results for ![a](http://www.sciweavers.org/tex2img.php?eq=%5Csigma%5E2%3D0&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0) and 2/3 of corresponding M/M/1 results for ![asdf](http://www.sciweavers.org/tex2img.php?eq=%5Csigma%5E2%20%3D%201%2F3%5Cmu%5E2&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0), in general, the results satisfied the formula: ![equation](http://www.sciweavers.org/tex2img.php?eq=E%28N%29%3D%5Cfrac%7B%5Crho%7D%7B1-%5Crho%7D%5B1-%5Cfrac%7B%5Crho%7D%7B2%7D%281-%5Cmu%5E2%5Csigma%5E2%29%5D&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0)

***Run experiment***

1. Pleas check the experiment environment:
    * *Ubuntu 14.04.4 LTS*
    * *Python 2.7.6* include *numpy* and *matplotlib* module
    * *NS2 -version 2.35*
    * check your *GENI* testbed *topology* as below
    * make sure you installed *D-ITG* on your *client* and *server* nodes
![top.png](https://bitbucket.org/repo/y4e7oM/images/3382968231-top.png)    
1. Download the codes under one folder, `cd /PATH/TO/folder` enter the folder
1. Run command `chmod a+x auto_ns.sh` to make it executable, then run `./auto_ns.sh`
1. Run command `ssh -i /PATH/TO/id_rsa USERNAME@HOSTNAME -p PORT` to login to your client, router, server nodes in separate terminal
1. Open a new terminal, run command `scp -i /PATH/TO/id_rsa -P PORT /PATH/TO/file USERNAME@HOSTNAME:~/` to upload *auto_tc.sh* and *queuemonitor.sh* to *router* node, *auto_ditg.sh* to *client* node, run command `chomod a+x XXX.sh` to make them executable
1. *First*, run `ITGRecv` on *server* node, *then* run `./auto_tc.sh` on *router* node, *at last* run `./auto_ditg.sh` on *client* node
1. After experiment, run command `scp -i /PATH/TO/id_rsa -P PORT USERNAME@HOSTNAME:~/lambda.txt /PATH/TO/folder` and `scp -i /PATH/TO/id_rsa -P PORT USERNAME@HOSTNAME:~/mu.txt /PATH/TO/folder` to download CSV from *client* node and run command `scp -i /PATH/TO/id_rsa -P PORT USERNAME@HOSTNAME:~/queue.txt /PATH/TO/folder` to download CSV from *router* node
1. Run command `python analyze.py` you will get the result images, if you want to validate how well measured ρ matches our expectation, run `python rho.py`, you will have image below:

![rho.png](https://bitbucket.org/repo/y4e7oM/images/657380386-rho.png)