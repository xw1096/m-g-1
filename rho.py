import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure(1)
rho = np.linspace(0.5, 0.95, 100)
l1 = plt.plot(rho, rho, 'r', label = 'Analytical')
data1 = np.loadtxt('nsmu.txt')
data2 = np.loadtxt('nslambda.txt')
data3 = np.loadtxt('mu.txt')
data4 = np.loadtxt('lambda.txt')
idealrho = [0.9216, 0.82, 0.7168, 0.6144, 0.512]
# geni testbed measured rho
#geni_rho225 = [data4[0:5]/data3[0:5], data4[25:30]/data3[25:30], data4[50:55]/data3[50:55]]
#x = np.linspace(idealrho[0], idealrho[0])
#y = np.linspace(np.min(geni_rho225), np.max(geni_rho225))
#l2 = plt.plot(x, y, 'b') 
#plt.show()
for i in [0, 1, 2, 3, 4]:
    geni_rho = [data4[0+5*i:5+5*i]/data3[0+5*i:5+5*i], data4[25+5*i:30+5*i]/data3[25+5*i:30+5*i], data4[50+5*i:55+5*i]/data3[50+5*i:55+5*i]]
    x = np.linspace(idealrho[i], idealrho[i])
    y = np.linspace(np.min(geni_rho), np.max(geni_rho))
    l = plt.plot(x, y, 'b')
    plt.plot(idealrho[i], np.mean(geni_rho), 'bo')
l2 = plt.plot(x, y, 'b', label = 'Testbed Experiment')
for i in [0, 1, 2, 3, 4]:
    ns_rho = [data2[0+5*i:5+5*i]/data1[0+5*i:5+5*i], data2[25+5*i:30+5*i]/data1[25+5*i:30+5*i], data2[50+5*i:55+5*i]/data1[50+5*i:55+5*i]]
    x = np.linspace(idealrho[i]+0.002, idealrho[i]+0.002)
    y = np.linspace(np.min(ns_rho), np.max(ns_rho))
    l = plt.plot(x, y, 'g')
    plt.plot(idealrho[i]+0.002, np.mean(ns_rho), 'go')
l3 = plt.plot(x, y, 'g', label = 'Simulation')
plt.xlim(0.5, 1.0)
plt.ylim(0.5, 1.0)
plt.xlabel(r'Ideal $\rho$')
plt.ylabel(r'Measured $\rho$')
plt.grid()
plt.legend(loc = 'upper left')
plt.savefig('rho.eps')
