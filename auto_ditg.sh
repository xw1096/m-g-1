#!/bin/bash

test=(225 200 175 150 125)
count1=(1 2 3 4 5)
count2=(1 2 3)

for j in ${count2[*]}
do
    for lambda in ${test[*]}
    do
        for i in ${count1[*]}
        do
            if [ $j -eq "1" ]
            then
            echo "start $lambda sigma 0 $i" | netcat router 8888
            ITGSend -a server -l sender.log -x receiver.log -E $lambda -c 512 -T UDP -t 85000
            elif [ $j -eq "2" ]
            then
            echo "start $lambda sigma 1/3 $i" | netcat router 8888
            ITGSend -a server -l sender.log -x receiver.log -E $lambda -u 1 1023 -T UDP -t 85000
            else
            echo "start $lambda sigma 1 $i" | netcat router 8888
            ITGSend -a server -l sender.log -x receiver.log -E $lambda -e 512 -T UDP -t 85000
            fi
            ITGDec sender.log | head -17 | tail -1 | awk '{ print $5 }' >> lambda.txt
            totalpackets=$(ITGDec sender.log | head -9 | tail -1 | awk '{print $4}')
            totalbytes=$(ITGDec sender.log | head -15 | tail -1 | awk '{print $4}')
            echo "scale=6;1000000*$totalpackets/$totalbytes/8"|bc >> mu.txt
        done
    done
done
